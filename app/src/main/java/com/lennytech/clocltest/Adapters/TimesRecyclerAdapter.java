package com.lennytech.clocltest.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lennytech.clocltest.Beans.TimeCard;
import com.lennytech.clocltest.Beans.TimeEntry;
import com.lennytech.clocltest.Listeners.CardClickLisgener;
import com.lennytech.clocltest.R;

/**
 * Created by mikybraun on 2017-10-29.
 */


/**
 * This adaper shows The All cards and the time entries based on what you pass in to it.
 */

public class TimesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private Object[] mItems;
    private CardClickLisgener mListener;
    private Context mContext;


    public TimesRecyclerAdapter(Context con, Object[] cards, CardClickLisgener listener) {
        mItems = cards;
        mListener = listener;
        mContext = con;
    }

    public void setNewData( Object[] cards) {
        mItems = null;
        mItems = cards;
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_cards_recycler_item, parent, false);
        return new TimeCardsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final TimeCardsViewHolder viewHolder = (TimeCardsViewHolder) holder;
       // Time Cards !!!!!
        if (mItems[position] instanceof TimeCard) {
            TimeCard card = (TimeCard) mItems[position];

            if (card.created_at != null)
                viewHolder.mDateTv.setText(card.occurrence);
            if (card.total_worked_hours != null)
                viewHolder.mTimeTv.setText(card.total_worked_hours + "h");

            viewHolder.mContainer.setTag(position);
            viewHolder.mContainer.setOnClickListener(this);

        } else {
            // Time Entries !!!!!
            TimeEntry entry = (TimeEntry) mItems[position];
            if ((mItems.length % 2) ==0 ){
                if ((position % 2) == 0) {
                    viewHolder.mDateTv.setText(mContext.getResources().getString(R.string.out));
                    if (entry.time != null) {
                        viewHolder.mTimeTv.setText(entry.time.split("T")[1]);
                    }
                } else {
                    viewHolder.mDateTv.setText(mContext.getResources().getString(R.string.in));
                    if (entry.time != null) {
                        viewHolder.mTimeTv.setText(entry.time.split("T")[1]);
                    }
                }
            }else{
                if ((position % 2) != 0) {
                    viewHolder.mDateTv.setText(mContext.getResources().getString(R.string.out));
                    if (entry.time != null) {
                        viewHolder.mTimeTv.setText(entry.time.split("T")[1]);
                    }
                } else {
                    viewHolder.mDateTv.setText(mContext.getResources().getString(R.string.in));
                    if (entry.time != null) {
                        viewHolder.mTimeTv.setText(entry.time.split("T")[1]);
                    }
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return mItems.length;
    }

    @Override
    public void onClick(View view) {
        if (mListener != null)
            mListener.onCardClicked((TimeCard) mItems[(int) view.getTag()]);
    }


    class TimeCardsViewHolder extends RecyclerView.ViewHolder {

        public TextView mDateTv, mTimeTv;
        public LinearLayout mContainer;


        public TimeCardsViewHolder(View itemView) {
            super(itemView);
            mDateTv = (TextView) itemView.findViewById(R.id.dateTv);
            mTimeTv = (TextView) itemView.findViewById(R.id.timeTv);
            mContainer = (LinearLayout) itemView.findViewById(R.id.container);
        }
    }
}

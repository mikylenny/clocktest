package com.lennytech.clocltest.Tasks;

import android.os.AsyncTask;

import com.lennytech.clocltest.Beans.TimeCard;
import com.lennytech.clocltest.Listeners.CreateTimesListener;
import com.lennytech.clocltest.Listeners.PrevDataListener;
import com.lennytech.clocltest.Listeners.TimeEntryCreatorListener;
import com.lennytech.clocltest.Network.Constants;
import com.lennytech.clocltest.Network.NetworkManager;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mikybraun on 2017-10-30.
 */

public class TimeEntryCreator extends AsyncTask<Void, Void, Void> implements CreateTimesListener, PrevDataListener {
    private TimeCard[] mHistory;
    private WeakReference<TimeEntryCreatorListener> mListener;
    private NetworkManager mNetManager;

    public TimeEntryCreator(TimeCard[] history, TimeEntryCreatorListener listener) {
        mHistory = history;
        mListener = new WeakReference<>(listener);
    }


    @Override
    protected Void doInBackground(Void... voids) {
        checkTimeCardAndCreatEntry();
        return null;
    }

    private void checkTimeCardAndCreatEntry() {
        mNetManager = NetworkManager.getInstance();
        int cardId = 0;

        for (int t = 0; t < mHistory.length; t++) {
            if (mHistory[t].username.equalsIgnoreCase(Constants.USER_NAME) && compareDates(mHistory[t].occurrence)) {
                cardId = mHistory[t].id;
                mNetManager.createTimeEntry(cardId, this);
                break;
            }
        }
        if (cardId == 0) {
            mNetManager.createTimeCard(this);
        }
    }


    private boolean compareDates(String occurrence) {
        Date dateObj = null;
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateObj = curFormater.parse(occurrence);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar org = Calendar.getInstance();
        org.setTime(dateObj);

        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DATE) == org.get(Calendar.DATE) && c.get(Calendar.MONTH) == org.get(Calendar.MONTH) && c.get(Calendar.YEAR) == org.get(Calendar.YEAR);
    }

    @Override
    public void onCreateTimeCardSuccess() {
        mNetManager.getPrevData(this);
        checkTimeCardAndCreatEntry();
    }

    @Override
    public void onCreateTimeEntrySuccess() {
        if (mListener.get() != null)
            mListener.get().onTimeEntryCreated();
    }

    @Override
    public void onListArrived(TimeCard[] allTimeCards) {
        mHistory = allTimeCards;
    }
}

package com.lennytech.clocltest.Activities;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lennytech.clocltest.Adapters.TimesRecyclerAdapter;
import com.lennytech.clocltest.Beans.TimeCard;
import com.lennytech.clocltest.Listeners.CardClickLisgener;
import com.lennytech.clocltest.Listeners.CreateTimesListener;
import com.lennytech.clocltest.Listeners.PrevDataListener;
import com.lennytech.clocltest.Listeners.TimeEntryCreatorListener;
import com.lennytech.clocltest.Network.Constants;
import com.lennytech.clocltest.Network.NetworkManager;
import com.lennytech.clocltest.R;
import com.lennytech.clocltest.Tasks.TimeEntryCreator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mikybraun on 2017-10-29.
 */


public class MainActivity extends AppCompatActivity implements View.OnClickListener, PrevDataListener, CardClickLisgener, TimeEntryCreatorListener {
    private Button mPunchInBtn, mPunchOutBtn;
    private RecyclerView RView;
    private NetworkManager mNetManager;
    private boolean mTimeEntriesShown, mTimeCardCreated;
    private TimesRecyclerAdapter mAdapter;
    private TimeCard[] mHistory;
    private TextView mDateTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNetManager = NetworkManager.getInstance();
        setContentView(R.layout.activity_main);
        initTools();
        initControllers();
        refreshList();
        ShowAllCards(true);
    }


    private void initTools() {
        mPunchInBtn = (Button) findViewById(R.id.punchInBtn);
        mPunchOutBtn = (Button) findViewById(R.id.punchOutBtn);
        RView = (RecyclerView) findViewById(R.id.RView);
        mDateTv = (TextView) findViewById(R.id.dateTv);
    }


    private void initControllers() {
        mPunchInBtn.setOnClickListener(this);
        mPunchOutBtn.setOnClickListener(this);
        RView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.punchOutBtn:
            case R.id.punchInBtn:
                CreateTimeEntry();
                break;
        }

    }

    private void CreateTimeEntry() {
        new TimeEntryCreator(mHistory,this).execute();
    }





    @Override
    public void onListArrived(TimeCard[] allTimeCards) {
        mHistory = allTimeCards;
        if(mTimeCardCreated){
            mTimeCardCreated = false;
            CreateTimeEntry();
        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mAdapter == null) {
                        mAdapter = new TimesRecyclerAdapter(MainActivity.this, mHistory, MainActivity.this);
                        RView.setAdapter(mAdapter);
                    } else {
                        mAdapter.setNewData(mHistory);
                    }

                }
            });
        }

    }

    @Override
    public void onCardClicked(TimeCard card) {
        mTimeEntriesShown = true;
        if (card.time_entries.size() == 0 || card.time_entries == null)
            Toast.makeText(this, "No time entries received from server", Toast.LENGTH_LONG).show();
        else {
            ShowAllCards(false);
            mDateTv.setText(card.occurrence);
            mAdapter.setNewData(card.time_entries.toArray());
            RView.setAdapter(mAdapter);
        }
    }


    private void ShowAllCards(boolean isAllCards) {
        if (isAllCards) {
            mDateTv.setVisibility(View.GONE);
            mPunchInBtn.setVisibility(View.VISIBLE);
            mPunchOutBtn.setVisibility(View.VISIBLE);
        } else {
            mDateTv.setVisibility(View.VISIBLE);
            mPunchInBtn.setVisibility(View.GONE);
            mPunchOutBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (mTimeEntriesShown) {
            mTimeEntriesShown = false;
            ShowAllCards(true);
            mAdapter.setNewData(mHistory);
            RView.setAdapter(mAdapter);
        } else
            super.onBackPressed();
    }


    private void refreshList() {
        mNetManager.getPrevData(this);
    }

    @Override
    public void onTimeEntryCreated() {
        refreshList();
    }
}

package com.lennytech.clocltest.Network.NetworkIntefaces;

import com.lennytech.clocltest.Beans.AllTimeCardsResponse;
import com.lennytech.clocltest.Beans.CreateTimeCardRequestObject;
import com.lennytech.clocltest.Beans.CreateTimeEntryRequestObject;
import com.lennytech.clocltest.Beans.EmptyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by mikybraun on 2017-10-29.
 */


/**
 *  retrofit 2 interface implementation
 */


public interface RetrofitInterface {

    @GET("timecards")
    Call<AllTimeCardsResponse> getAllTimeCards();

    @POST("timecards")
    Call<EmptyResponse> createTimeCard(@Body CreateTimeCardRequestObject body);

    @POST("time_entries")
    Call<EmptyResponse> createTimeEntry(@Body CreateTimeEntryRequestObject body);

}

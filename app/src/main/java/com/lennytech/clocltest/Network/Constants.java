package com.lennytech.clocltest.Network;

/**
 * Created by mikybraun on 2017-10-29.
 */

public class Constants {
    public static final String BASE_URL = "https://timekeeping-api.herokuapp.com/api/v1/";
    public static final String USER_NAME = "Miky Test";
}

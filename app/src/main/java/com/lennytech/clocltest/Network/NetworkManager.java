package com.lennytech.clocltest.Network;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.lennytech.clocltest.Beans.AllTimeCardsResponse;
import com.lennytech.clocltest.Beans.CreateTimeCardRequestObject;
import com.lennytech.clocltest.Beans.CreateTimeEntryRequestObject;
import com.lennytech.clocltest.Beans.EmptyResponse;
import com.lennytech.clocltest.Beans.TimeCard;
import com.lennytech.clocltest.Listeners.CreateTimesListener;
import com.lennytech.clocltest.Listeners.PrevDataListener;
import com.lennytech.clocltest.Network.NetworkIntefaces.RetrofitInterface;

import java.lang.ref.WeakReference;
import java.util.Date;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mikybraun on 2017-10-29.
 */

public class NetworkManager {

    private static NetworkManager instance;
    private RetrofitInterface mConnector;

    private NetworkManager() {
        initRetrofit();
    }

    public static NetworkManager getInstance() {
        if (instance == null) {
            instance = new NetworkManager();
        }
        return instance;
    }

    private void initRetrofit() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(Constants.BASE_URL).addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.client(httpClient.build()).build();
        mConnector = retrofit.create(RetrofitInterface.class);
    }

    public void getPrevData(final PrevDataListener listener) {
        mConnector.getAllTimeCards().enqueue(new Callback<AllTimeCardsResponse>() {
            @Override
            public void onResponse(Call<AllTimeCardsResponse> call, Response<AllTimeCardsResponse> response) {
                new JsonParser(listener, response.body().timecards).execute();
            }

            @Override
            public void onFailure(Call<AllTimeCardsResponse> call, Throwable t) {
                Log.e("Miky", "Failed");
            }
        });
    }


    public void createTimeEntry(int cardId, final CreateTimesListener listener) {
        mConnector.createTimeEntry(new CreateTimeEntryRequestObject(cardId, new Date(System.currentTimeMillis()))).enqueue(new Callback<EmptyResponse>() {
            @Override
            public void onResponse(Call<EmptyResponse> call, Response<EmptyResponse> response) {
                if (listener != null)
                    listener.onCreateTimeEntrySuccess();
            }

            @Override
            public void onFailure(Call<EmptyResponse> call, Throwable t) {
                Log.e("Miky", "Failed");
            }
        });
    }


    public void createTimeCard(final CreateTimesListener listener) {
        mConnector.createTimeCard(new CreateTimeCardRequestObject(Constants.USER_NAME, new Date(System.currentTimeMillis()))).enqueue(new Callback<EmptyResponse>() {
            @Override
            public void onResponse(Call<EmptyResponse> call, Response<EmptyResponse> response) {
                if (listener != null)
                    listener.onCreateTimeEntrySuccess();
            }

            @Override
            public void onFailure(Call<EmptyResponse> call, Throwable t) {
                Log.e("Miky", "Failed");
            }
        });
    }


    class JsonParser extends AsyncTask<Void, Void, Void> {
        private WeakReference<PrevDataListener> ilistener;
        private String iJson;

        public JsonParser(PrevDataListener listener, String json) {
            ilistener = new WeakReference<>(listener);
            iJson = json;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            parseTheJson();
            return null;
        }

        private void parseTheJson() {
            Gson gson = new Gson();
            if (ilistener.get() != null)
                ilistener.get().onListArrived(gson.fromJson(iJson, TimeCard[].class));

        }
    }
}

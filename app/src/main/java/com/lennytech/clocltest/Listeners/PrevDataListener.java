package com.lennytech.clocltest.Listeners;

import com.lennytech.clocltest.Beans.TimeCard;

/**
 * Created by mikybraun on 2017-10-29.
 */

public interface PrevDataListener {
    void onListArrived(TimeCard[] allTimeCards);
}

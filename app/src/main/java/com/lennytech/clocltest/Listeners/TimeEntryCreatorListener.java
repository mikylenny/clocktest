package com.lennytech.clocltest.Listeners;

/**
 * Created by mikybraun on 2017-10-30.
 */

public interface TimeEntryCreatorListener {
    void onTimeEntryCreated();
}

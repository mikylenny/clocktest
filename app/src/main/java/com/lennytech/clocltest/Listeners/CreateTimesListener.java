package com.lennytech.clocltest.Listeners;

/**
 * Created by mikybraun on 2017-10-29.
 */

public interface CreateTimesListener {
    void onCreateTimeCardSuccess();
    void onCreateTimeEntrySuccess();
}

package com.lennytech.clocltest.Beans;

import java.util.Date;

/**
 * Created by mikybraun on 2017-10-29.
 */

public class CreateTimeCardRequestObject {
    public String username;
    public Date occurrence;

    public CreateTimeCardRequestObject(String userName, Date date){
        this.username = userName;
        this.occurrence = date;
    }
}

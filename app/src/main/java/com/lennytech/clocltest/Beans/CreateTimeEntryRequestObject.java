package com.lennytech.clocltest.Beans;

import java.util.Date;

/**
 * Created by mikybraun on 2017-10-29.
 */

public class CreateTimeEntryRequestObject {
    public Date time;
    public int timecard_id;

    public CreateTimeEntryRequestObject(int timeCardId, Date date){
        this.timecard_id = timeCardId;
        this.time = date;
    }
}

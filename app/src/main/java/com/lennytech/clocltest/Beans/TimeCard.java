package com.lennytech.clocltest.Beans;

import java.util.List;

/**
 * Created by mikybraun on 2017-10-29.
 */

public class TimeCard {
    public int id;
    public String occurrence;
    public String username;
    public String total_worked_hours;
    public boolean exception;
    public String created_at;
    public String updated_at;
    public List<TimeEntry> time_entries;
}
